# Test

A test task on a pre-built design using **Vue, Vuex, Bootstrap**. <br />
 A one-page application that allows you to retrieve, display and filter data from an external REST API.

_Live demo you can watch [here](https://arkhipova.gitlab.io/test/). <br />_

## Build Setup

```bash
# clone repo
git clone https://gitlab.com/Arkhipova/test.git

# install dependencies
npm i

# serve with hot reload at http://localhost:8080/
npm run serve

```
