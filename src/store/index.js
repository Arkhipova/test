import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    posts: [],
    users: [],
  },

  mutations: {
    fillPosts(state, posts) {
      state.posts = posts;
    },
    fillUsers(state, users) {
      state.users = users;
    },
  },

  actions: {
    async fetchPosts(ctx, limit) {
      let response = await axios.get(
        'http://jsonplaceholder.typicode.com/posts?_limit=' + limit
      );
      this.commit('fillPosts', response.data);
    },
    async fetchUsers(ctx) {
      let response = await axios.get(
        'http://jsonplaceholder.typicode.com/users'
      );
      this.commit('fillUsers', response.data);
    },
  },

  getters: {
    allPosts(state) {
      if (state.users.length === 0) return []; // To prevent an error before users have loaded
      let arr = [];
      state.posts.forEach((el) => {
        arr.push({
          id: el.id,
          title: el.title,
          body: el.body,
          author: state.users.find((user) => {
            return el.userId === user.id;
          }).name,
        });
      });
      return arr;
    },
  },
});
